# imports
import speech_recognition as sr
from time import sleep
from datetime import datetime
import webbrowser
import pyttsx3
import winsound

frequency = 200
duration = 100
USERNAME = "Chris"

r = sr.Recognizer()
engine = pyttsx3.init()


# speak tts
def speak(text):
    engine.say(text)
    engine.runAndWait()


options = {
    "who":
        {
            "you": "My name is PythonBot",
            "i": "Your name is " + USERNAME,
            "me": "Your name is " + USERNAME
        },
    "what":
        {
            "time": "The time is " + datetime.now().time().strftime("%H %M"),
            "hour": "The time is " + datetime.now().time().strftime("%H %M"),
            "day": "The day is " + datetime.now().strftime("%d %B %Y"),
            "date": "The day is " + datetime.now().strftime("%d %B %Y")
        },
    "search":
        {
            "google": "https://google.com/search?q=",
            "bing": "https://www.bing.com/search/?q=",
            "reddit": "https://www.reddit.com/search/?q=",
            "red": "https://www.reddit.com/search/?q="
        }
}


def recognize_voice():
    text = ''

    with sr.Microphone() as source:
        # set for change in ambient noise
        r.adjust_for_ambient_noise(source)

        winsound.Beep(frequency, duration)
        # capture the voice
        voice = r.listen(source)

        try:
            text = r.recognize_google(voice)
        except sr.RequestError:
            speak("Sorry, cannot access Google API...")
        except sr.UnknownValueError:
            speak("Sorry, unable to recognize speech...")
    return text.lower()


# function to respond to the user
# text_version: string of users voice
def reply(text_version):
    print(text_version)
    if "name" in text_version:
        speak("My name is Chris")

    if "how are you" in text_version:
        speak("I am fine...")

    if "date" in text_version:
        date = datetime.now().strftime("%d %B %Y")
        speak(date)

    if "time" in text_version:
        time = datetime.now().time().strftime("%H %M")
        speak("The time is " + time)

    if "search" in text_version:
        speak("What do you want me to search for?")
        keyword = recognize_voice()

        if keyword != '':
            url = "https://google.com/search?q=" + keyword

            speak("Here are the results for " + keyword)

            webbrowser.open(url)
            sleep(3)

    if "quit" in text_version or "exit" in text_version:
        speak("Okay, I am going to sleep now...")
        exit()


def reply2(text_version):
    global options
    for option in options.keys():
        if option in text_version:
            if option == 'search':
                voice_search()
            else:
                for response in options[option].keys():
                    if response in text_version:
                        speak(options[option][response])
    if "quit" in text_version or "exit" in text_version:
        speak("Okay, I am going to sleep now...")
        exit()


def voice_search():
    global options
    speak("Which search engine would you like...")

    response = recognize_voice()

    for engines in options["search"].keys():
        if engines in response:
            speak("What would you like to search for...")
            response = recognize_voice()
            url = options['search'][engines] + response
            webbrowser.open(url)


sleep(1)

while True:
    speak("Start speaking...")
    # listen for voice and convert it into text format
    text_speech = recognize_voice()

    reply2(text_speech)
